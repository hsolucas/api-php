# Código fonte em PHP para criação de API RESTful simples

## Configurar conexão com banco de dados
* Criar o diretório `properties` e o arquivo `properties/bd.properties` na raiz do projeto e incluir os parâmetros:
```
dbtype:[mysql][sqlsrv][pgsql]
host:127.0.0.1
port:3306
user:root
password:yourpassword
dbname:yourdbname
```

## Suporte a logs
* Criar na raiz do projeto o diretório `logs` com permissão de leitura e escrita para o usuário "apache"

## Definir endpoints
* Alterar o método getFunctionClass() da classe APIMethod em classes/api para que retorne um array associado por chave/valor, onde a chave é o nome da URL desejada e o valor é a classe em classes/model que deverá ser retornada.

> //Exemplo:
    protected static function getFunctionClass(){
		$functions = array(
			"produtos" => "Produto", //https://minhaapi.com.br/produtos
			"clientes" => "Cliente" //https://minhaapi.com.br/clientes
		);
        return $functions;
    }

## Mapeamento de tabelas / Criação de Models
* Criar em classes/model uma classe PHP para cada tabela/view do banco de dados.
* A classe criada deve herdar de GenericDAO, a qual já contém todos os métodos necessários para manipular os dados na transação com o banco. Alguns desses métodos podem ser sobrescritos para adicionar funcionalidades específicas. 
* No método construtor, devem ser enviados à classe pai:
{tableName:string} [Nome da tabela no banco](Requerido)
{columns:array} [Array associativo com o mapeamento das colunas da tabela para as propriedades da classe, onde a chave é o nome da coluna da tabela e o valor é o nome da propriedade da classe](Requerido)
{oneMany:array} [Array associativo com as chaves estrangeiras 1-1 e 1-N, onde a chave é o nome da propriedade da classe e o valor é o nome da classe referenciada](Opcional)
{manyOne:array} [Array associativo com as chaves estrangeiras N-1, onde a chave é o nome da propriedade da classe (nome da lista) e o valor é o nome da classe referenciada](Opcional)
{manyOneId:string} [Nome da propriedade da classe referenciada](Requerido somente se existe manyOne)

> //Exemplo:
	public function __construct(){
		$tableName = "TB_CLIENTE";
		$columns = array(
			"ID_CLIENTE" => "id",
			"NM_NOME" => "nome",
			"NM_EMAIL" => "email"
		);
		$oneMany = array(
			"genero" => "Genero", //1-1
			"estCivil" => "EstadoCivil" //1-1
		);
		$manyOne = array(
			"comprasRealizadas" => "Compra" //N Compras-1 Cliente
		);
		$manyOneId = "idCliente"; //idCliente deve estar declarado na classe Compra 
        parent::__construct($tableName, $columns, $oneMany, $manyOne, $manyOneId);
	}
